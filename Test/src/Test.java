import java.util.HashMap;
import java.util.Map;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		String str = "abc";
        boolean result = validPalindrome(str);
        System.out.println(result);

	}
	  @SuppressWarnings("unlikely-arg-type")
	public static boolean validPalindrome(String str){
	       
	        HashMap<String, Integer> result = new HashMap<String, Integer>();
	        if(str == null || str.isEmpty()){
	            return false;
	        }
	        
	        for(int i = 0; i < str.length(); i++){
	            if(result.containsKey(str.charAt(i)))
	                result.put(Character.toString(str.charAt(i)),result.get(Character.toString(str.charAt(i)))+1) ;
	            else   
	                result.put(Character.toString(str.charAt(i)), 1);
	        }
	        
	        int counter = 0;
	        for (Map.Entry<String, Integer> entry : result.entrySet()) {
	        	if(entry.getValue() % 2 != 0)
	                counter++;
	        }
	        
	        if(counter > 1)
	            return false;
	        else{
	            return true;
	        }
	     
	     }
}
